package BP.Frm;

import BP.En.EntityMyPKAttr;

public class CtrlModelAttr extends EntityMyPKAttr {
    /**
     * 表单ID
     */
    public static final String FrmID = "FrmID";
    /**


     */
    public static final String CtrlObj = "CtrlObj";
    /**
     * 所有的人
     */
    public static final String IsEnableAll = "IsEnableAll";
    public static final String IsEnableStation = "IsEnableStation";
    public static final String IsEnableDept = "IsEnableDept";
    public static final String IsEnableUser = "IsEnableUser";

    public static final String IDOfUsers = "IDOfUsers";
    public static final String IDOfStations = "IDOfStations";
    public static final String IDOfDepts = "IDOfDepts";

}
