package BP.Web.Controls;

import BP.Web.*;

/**
 * DDLShowType
 */
public enum DDLShowType_del {
    /**
     * None
     */
    None(0),
    /**
     * Gender
     */
    Gender(1),
    /**
     * Boolean
     */
    Boolean(2),
    /**
     * Year
     */
    Year(3),
    /**
     * Month
     */
    Month(4),
    /**
     * Day
     */
    Day(5),
    /**
     * hh
     */
    hh(6),
    /**
     * mm
     */
    mm(7),
    /**
     * 季度
     */
    Quarter(8),
    /**
     * Week
     */
    Week(9),
    /**
     * 系统枚举类型 SelfBindKey="系统枚举Key"
     */
    SysEnum(10),
    /**
     * Self
     */
    Self(11),
    /**
     * 实体集合
     */
    Ens(12),
    /**
     * 与Table 相关联
     */
    BindTable(13);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, DDLShowType_del> mappings;
    private int intValue;

    private DDLShowType_del(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, DDLShowType_del> getMappings() {
        if (mappings == null) {
            synchronized (DDLShowType_del.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, DDLShowType_del>();
                }
            }
        }
        return mappings;
    }

    public static DDLShowType_del forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
