package BP.En;

import BP.Web.Controls.*;
import BP.DA.*;

import java.io.*;
import java.time.*;
import java.math.*;

/**
 * 逻辑类型
 */
public enum FieldTypeS {
    /**
     * 普通类型
     */
    Normal(0),
    /**
     * 枚举类型
     */
    Enum(1),
    /**
     * 外键
     */
    FK(2);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, FieldTypeS> mappings;
    private int intValue;

    private FieldTypeS(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, FieldTypeS> getMappings() {
        if (mappings == null) {
            synchronized (FieldTypeS.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, FieldTypeS>();
                }
            }
        }
        return mappings;
    }

    public static FieldTypeS forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
