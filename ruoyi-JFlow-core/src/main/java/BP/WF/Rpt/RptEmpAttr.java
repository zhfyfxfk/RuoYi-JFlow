package BP.WF.Rpt;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import BP.WF.*;

import java.util.*;

/**
 * 报表人员
 */
public class RptEmpAttr {

    ///#region 基本属性
    /**
     * 报表ID
     */
    public static final String FK_Rpt = "FK_Rpt";
    /**
     * 人员
     */
    public static final String FK_Emp = "FK_Emp";

    ///#endregion
}
