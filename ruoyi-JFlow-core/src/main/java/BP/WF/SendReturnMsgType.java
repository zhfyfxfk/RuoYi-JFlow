package BP.WF;

/**
 * 消息类型
 */
public enum SendReturnMsgType {
    /**
     * 消息
     */
    Info,
    /**
     * 系统消息
     */
    SystemMsg;

    public static final int SIZE = java.lang.Integer.SIZE;

    public static SendReturnMsgType forValue(int value) {
        return values()[value];
    }

    public int getValue() {
        return this.ordinal();
    }
}
