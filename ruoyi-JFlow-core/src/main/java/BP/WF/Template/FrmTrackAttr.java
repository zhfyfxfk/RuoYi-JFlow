package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Template.*;
import BP.WF.*;
import BP.Sys.*;
import BP.WF.*;

import java.util.*;

/**
 * 轨迹图标组件
 */
public class FrmTrackAttr extends EntityNoAttr {
    /**
     * 显示标签
     */
    public static final String FrmTrackLab = "FrmTrackLab";
    /**
     * 状态
     */
    public static final String FrmTrackSta = "FrmTrackSta";
    /**
     * X
     */
    public static final String FrmTrack_X = "FrmTrack_X";
    /**
     * Y
     */
    public static final String FrmTrack_Y = "FrmTrack_Y";
    /**
     * H
     */
    public static final String FrmTrack_H = "FrmTrack_H";
    /**
     * W
     */
    public static final String FrmTrack_W = "FrmTrack_W";
}
