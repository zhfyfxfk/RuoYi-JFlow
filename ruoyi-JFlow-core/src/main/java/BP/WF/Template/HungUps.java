package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.*;
import BP.WF.Template.*;
import BP.Port.*;
import BP.WF.*;

import java.util.*;

/**
 * 挂起
 */
public class HungUps extends EntitiesMyPK {

    ///#region 方法

    /**
     * 挂起
     */
    public HungUps() {
    }

    /**
     * 得到它的 Entity
     */
    @Override
    public Entity getNewEntity() {
        return new HungUp();
    }

    ///#endregion


    ///#region 为了适应自动翻译成java的需要,把实体转换成List.

    /**
     * 转化成 java list,C#不能调用.
     *
     * @return List
     */
    public final List<HungUp> ToJavaList() {
        return (List<HungUp>) (Object) this;
    }

    /**
     * 转化成list
     *
     * @return List
     */
    public final ArrayList<HungUp> Tolist() {
        ArrayList<HungUp> list = new ArrayList<HungUp>();
        for (int i = 0; i < this.size(); i++) {
            list.add((HungUp) this.get(i));
        }
        return list;
    }

    ///#endregion 为了适应自动翻译成java的需要,把实体转换成List.
}
