package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Template.*;
import BP.WF.*;
import BP.Sys.*;
import BP.WF.*;

import java.util.*;

/**
 * 子流程类型
 */
public enum SubFlowType {
    /**
     * 手动的子流程
     */
    HandSubFlow(0),
    /**
     * 自动触发的子流程
     */
    AutoSubFlow(1),
    /**
     * 延续子流程
     */
    YanXuFlow(2);

    public static final int SIZE = java.lang.Integer.SIZE;
    private static java.util.HashMap<Integer, SubFlowType> mappings;
    private int intValue;

    private SubFlowType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    private static java.util.HashMap<Integer, SubFlowType> getMappings() {
        if (mappings == null) {
            synchronized (SubFlowType.class) {
                if (mappings == null) {
                    mappings = new java.util.HashMap<Integer, SubFlowType>();
                }
            }
        }
        return mappings;
    }

    public static SubFlowType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
