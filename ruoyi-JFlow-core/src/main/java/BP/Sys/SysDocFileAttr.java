package BP.Sys;

import BP.DA.*;
import BP.En.*;

import java.util.*;
import java.io.*;

public class SysDocFileAttr {
    /**
     * 关联的Table
     */
    public static final String EnName = "EnName";
    /**
     * 关联的key
     */
    public static final String RefKey = "RefKey";
    /**
     * 主键值
     */
    public static final String RefVal = "RefVal";
    /**
     * 文件名称
     */
    public static final String FileName = "FileName";
    /**
     * 文件大小
     */
    public static final String FileSize = "FileSize";
    /**
     * 文件类型
     */
    public static final String FileType = "FileType";
}
