package BP.Sys;

import BP.DA.*;
import BP.En.*;

import java.util.*;
import java.math.*;

/**
 * 全局变量
 */
public class GloVarAttr extends EntityNoNameAttr {
    /**
     * Val
     */
    public static final String Val = "Val";
    /**
     * Note
     */
    public static final String Note = "Note";
    /**
     * GroupKey
     */
    public static final String GroupKey = "GroupKey";
    /**
     * 顺序号
     */
    public static final String Idx = "Idx";
}
