package BP.Sys;

import BP.En.*;

/**
 * Excel数据表
 */
public class ExcelTable extends EntityNoName {

    ///#region 属性
    ///#region 构造方法
    public ExcelTable() {
    }

    /**
     * 获取或设置Excel模板
     *
     * @throws Exception
     */
    public final String getFK_ExcelFile() throws Exception {
        return this.GetValStrByKey(ExcelTableAttr.FK_ExcelFile);
    }

    public final void setFK_ExcelFile(String value) throws Exception {
        this.SetValByKey(ExcelTableAttr.FK_ExcelFile, value);
    }

    /**
     * 获取或设置是否明细表
     *
     * @throws Exception
     */
    public final boolean getIsDtl() throws Exception {
        return this.GetValBooleanByKey(ExcelTableAttr.IsDtl);
    }

    public final void setIsDtl(boolean value) throws Exception {
        this.SetValByKey(ExcelTableAttr.IsDtl, value);
    }

    /**
     * 获取或设置数据表说明
     *
     * @throws Exception
     */
    public final String getNote() throws Exception {
        return this.GetValStrByKey(ExcelTableAttr.Note);
    }

    public final void setNote(String value) throws Exception {
        this.SetValByKey(ExcelTableAttr.Note, value);
    }

    /**
     * 获取或设置同步到表
     *
     * @throws Exception
     */
    public final String getSyncToTable() throws Exception {
        return this.GetValStrByKey(ExcelTableAttr.SyncToTable);
    }


    ///#endregion 属性

    public final void setSyncToTable(String value) throws Exception {
        this.SetValByKey(ExcelTableAttr.SyncToTable, value);
    }

    ///#endregion 构造方法

    /**
     * Excel数据表Map
     */
    @Override
    public Map getEnMap() {
        if (this.get_enMap() != null) {
            return this.get_enMap();
        }

        Map map = new Map("Sys_ExcelTable");
        map.setEnDesc("Excel数据表");

        map.AddTBStringPK(ExcelTableAttr.No, null, "编号", true, true, 1, 36, 200);
        map.AddTBString(ExcelTableAttr.Name, null, "数据表名", true, false, 1, 50, 100);
        map.AddDDLEntities(ExcelTableAttr.FK_ExcelFile, null, "Excel模板", new ExcelFiles(), true);
        map.AddBoolean(ExcelTableAttr.IsDtl, false, "是否明细表", true, false);
        map.AddTBStringDoc(ExcelTableAttr.Note, null, "数据表说明", true, false, true);
        map.AddTBString(ExcelTableAttr.SyncToTable, null, "同步到表", true, false, 1, 100, 100);

        this.set_enMap(map);
        return this.get_enMap();
    }

    ///#endregion 权限控制


    ///#region EnMap

    ///#region 权限控制
    @Override
    public UAC getHisUAC() {
        UAC uac = new UAC();
        uac.OpenAll();
        return uac;
    }

    ///#endregion EnMap


    ///#region 重写事件

    /**
     * 记录添加前事件
     *
     * @throws Exception
     */
    @Override
    protected boolean beforeInsert() throws Exception {
        return super.beforeInsert();
    }


    ///#endregion 重写事件
}
